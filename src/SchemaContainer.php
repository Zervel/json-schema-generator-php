<?php

namespace OpenapiNextGeneration\JsonSchemaGeneratorPhp;

class SchemaContainer
{
    /**
     * @var string
     */
    protected $path;
    /**
     * @var array
     */
    protected $schema;


    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setSchema(array $schema): void
    {
        $this->schema = $schema;
    }

    public function getSchema(): array
    {
        return $this->schema;
    }

    public function getSchemaAsJson(): string
    {
        return json_encode($this->schema, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }
}